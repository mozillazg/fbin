package cos

import (
	"context"
	"errors"
	"fmt"
	"io"
	"net/http"
	"strings"
	"time"

	"github.com/mozillazg/fbin/storage"
	cossdk "github.com/mozillazg/go-cos"
)

const name = "cos"
const timeout = time.Minute

type COS struct {
	client *cossdk.Client

	directory string
}

func init() {
	storage.Registry(name, factory)
}

func factory(opt storage.Option) (storage.Storage, error) {
	var bu string
	bucketURL := opt["bucket_url"]
	if bucketURL == nil {
		return nil, errors.New("option 'bucket_url' is missing")
	}
	if v, ok := bucketURL.(string); !ok || v == "" {
		return nil, errors.New("value of option 'bucket_url' is invalid")
	} else {
		bu = v
	}

	var si string
	secretID := opt["secret_id"]
	if secretID == nil {
		return nil, errors.New("option 'secret_id' is missing")
	}
	if v, ok := secretID.(string); !ok || v == "" {
		return nil, errors.New("value of option 'secret_id' is invalid")
	} else {
		si = v
	}

	var sk string
	secretKey := opt["secret_key"]
	if secretKey == nil {
		return nil, errors.New("option 'secret_key' is missing")
	}
	if v, ok := secretKey.(string); !ok || v == "" {
		return nil, errors.New("value of option 'secret_key' is invalid")
	} else {
		sk = v
	}

	var tm time.Duration
	if _, ok := opt["timeout"]; !ok {
		tm = timeout
	} else if v, ok := opt["timeout"].(int); !ok {
		return nil, errors.New("value of option 'timeout' is invalid")
	} else {
		tm = time.Second * time.Duration(v)
	}

	dt := ""
	directory := opt["directory"]
	if directory != nil {
		if v, ok := directory.(string); !ok {
			return nil, errors.New("value of option 'directory' is invalid")
		} else {
			dt = strings.Trim(v, "/")
		}
	}

	c, err := New(bu, si, sk, tm)
	if err != nil {
		return nil, err
	}
	c.directory = dt
	return c, nil
}

func New(bucketURL string, secretID, secretKey string, timeout time.Duration) (*COS, error) {
	b, err := cossdk.NewBaseURL(bucketURL)
	if err != nil {
		return nil, err
	}
	cl := cossdk.NewClient(b, &http.Client{
		Transport: &cossdk.AuthorizationTransport{
			SecretID:  secretID,
			SecretKey: secretKey,
		},
		Timeout: timeout,
	})
	return &COS{
		client: cl,
	}, nil
}

func (c *COS) Name() string {
	return name
}

func (c *COS) Upload(ctx context.Context, r io.Reader, p string, size int64) (string, error) {
	key := strings.Trim(
		fmt.Sprintf("%s/%s", c.directory, strings.Trim(p, "/")), "/")
	_, err := c.client.Object.Put(ctx, key, r, &cossdk.ObjectPutOptions{
		ObjectPutHeaderOptions: &cossdk.ObjectPutHeaderOptions{
			ContentLength: int(size),
		}})
	return key, err
}
