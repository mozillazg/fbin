package storage

import (
	"context"
	"fmt"
	"io"
)

type Storage interface {
	Name() string
	Upload(ctx context.Context, r io.Reader, p string, size int64) (string, error)
}

type Option map[string]interface{}
type Factory func(opt Option) (Storage, error)

var storages = map[string]Factory{}

func New(name string, opt Option) (Storage, error) {
	f, ok := storages[name]
	if !ok {
		return nil, fmt.Errorf("there is not an storage named %s", name)
	}

	return f(opt)
}

func Registry(name string, f Factory) error {
	if _, ok := storages[name]; ok {
		return fmt.Errorf("there is already exist an storage named %s", name)
	}

	storages[name] = f
	return nil
}
