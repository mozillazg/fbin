package fbin

import (
	"context"
	"fmt"
	"io/ioutil"
	"log"
	"math/rand"
	"mime/multipart"
	"net/http"
	"strings"
	"time"

	"github.com/google/uuid"
	"github.com/mozillazg/fbin/storage"
)

type handler func(w http.ResponseWriter, r *http.Request)

var defaultMaxFormMemory int64 = 1024 * 1024 * 10 // 10 MB
var defaultMaxBodySize int64 = 1024 * 1024 * 100  // 100 MB

type Server struct {
	config        serviceConfig
	mainStorage   storage.Storage
	extraStorages []storage.Storage
}

func New(config serviceConfig, mainStorage storage.Storage, extraStorages []storage.Storage) *Server {
	return &Server{
		config:        config,
		mainStorage:   mainStorage,
		extraStorages: extraStorages,
	}
}

func init() {
	rand.Seed(time.Now().UnixNano())
}

func (s *Server) ListenAndServe(addr string) error {
	http.HandleFunc("/api/v1/files", s.handleUpload())
	return http.ListenAndServe(addr, nil)
}

func (s *Server) handleUpload() handler {
	return func(w http.ResponseWriter, r *http.Request) {
		cAddr := cleanCAddr(r.RemoteAddr)
		log.Printf(`%s "%s %s"`, cAddr, r.Method, r.URL.Path)

		if r.Method != http.MethodPost {
			w.Header().Add("Allow", http.MethodPost)
			w.WriteHeader(http.StatusMethodNotAllowed)
			return
		}
		if !s.checkToken(w, r) {
			return
		}

		r.Body = http.MaxBytesReader(w, r.Body, s.config.MaxBodySizeB())
		if err := r.ParseMultipartForm(s.config.MaxFormMemoryB()); err != nil {
			log.Printf("parse form from %s failed: %s", cAddr, err)
			w.WriteHeader(http.StatusBadRequest)
			return
		}
		if r.MultipartForm == nil {
			w.WriteHeader(http.StatusBadRequest)
			w.Write([]byte(`form field "file" is needed`))
			return
		}

		files := r.MultipartForm.File["file"]
		if len(files) == 0 {
			w.WriteHeader(http.StatusBadRequest)
			w.Write([]byte(`form field "file" is needed`))
			return
		}

		if len(files) > 1 {
			w.WriteHeader(http.StatusBadRequest)
			w.Write([]byte(`form field "file" only allow once`))
			return
		}

		var err error
		fsize := files[0].Size
		f, err := files[0].Open()
		if err != nil {
			log.Printf("open parsed form file from %s failed: %s", cAddr, err)
			w.WriteHeader(http.StatusInternalServerError)
			return
		}

		path := genPathName()
		ctx := r.Context()
		key := ""
		fWrapper := ioutil.NopCloser(f)
		if key, err = s.mainStorage.Upload(ctx, fWrapper, path, fsize); err != nil {
			log.Printf("upload form file from %s to %s failed: %s",
				cAddr, s.mainStorage.Name(), err)
			f.Close()
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		go s.uploadToExtra(context.TODO(), f, path, fsize, cAddr)

		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusOK)
		w.Write([]byte(fmt.Sprintf(`{"key": "%s"}`, key)))
	}
}

func (s *Server) uploadToExtra(ctx context.Context, f multipart.File,
	path string, fsize int64, remoteAddr string) {
	defer f.Close()
	for _, st := range s.extraStorages {
		if _, err := f.Seek(0, 0); err != nil {
			log.Printf("seek file to begin failed: %s", err)
			continue
		}
		fWrapper := ioutil.NopCloser(f)
		if _, err := st.Upload(ctx, fWrapper, path, fsize); err != nil {
			log.Printf("upload form file from %s to %s failed: %s",
				remoteAddr, st.Name(), err)
		}
	}
}

func (s *Server) checkToken(w http.ResponseWriter, r *http.Request) bool {
	if r.Header.Get("Token") != s.config.Auth.Token {
		w.WriteHeader(http.StatusForbidden)
		return false
	}
	return true
}

func genPathName() string {
	return uuid.New().String()
}

func cleanCAddr(addr string) string {
	return strings.Split(addr, ":")[0]
}
