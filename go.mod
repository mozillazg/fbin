module github.com/mozillazg/fbin

require (
	github.com/BurntSushi/toml v0.3.1
	github.com/google/uuid v1.0.0
	github.com/mozillazg/go-cos v0.10.0
)
