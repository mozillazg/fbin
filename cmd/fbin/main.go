package main

import (
	"context"
	"log"
	"os"
	"os/signal"
	"syscall"

	"github.com/mozillazg/fbin"
	"github.com/mozillazg/fbin/storage"

	_ "github.com/mozillazg/fbin/storage/cos"
)

const version = "0.1.0"

var gitCommit = "unknown"
var built = "unknown"

func main() {
	ctx := context.Background()
	f := os.Args[1]
	cfg := &fbin.Config{}
	if err := fbin.UnmarshalFile(f, cfg); err != nil {
		log.Fatalf("read config (%s) failed: %s", f, err)
	}
	var mainStorage storage.Storage
	var extraStorages []storage.Storage
	for name, opt := range cfg.Storage.Services {
		if st, err := storage.New(name, opt); err != nil {
			log.Fatalf("init storage %#v failed: %s", name, err)
		} else {
			if name == cfg.Storage.MainService {
				mainStorage = st
			} else {
				extraStorages = append(extraStorages, st)
			}
		}
	}

	server := fbin.New(cfg.Service, mainStorage, extraStorages)
	log.Printf("listen and serve on %s", cfg.Service.Listen)
	ctx, cancel := context.WithCancel(ctx)
	cE := make(chan error, 1)
	cS := make(chan os.Signal, 1)
	signal.Notify(cS, os.Interrupt, syscall.SIGQUIT, syscall.SIGTERM, syscall.SIGHUP)

	go func() {
		c := <-cS
		log.Printf("received %#v signal program will exist, bye bye", c.String())
		cancel()
	}()
	go func() {
		if err := server.ListenAndServe(cfg.Service.Listen); err != nil {
			log.Printf("listen and serve on %s failed: %s", cfg.Service.Listen, err)
			cE <- err
			cancel()
		}
	}()

	select {
	case err := <-cE:
		log.Fatalf("program will exit because of error: %s", err)
	case <-ctx.Done():
		log.Println("bye bye")
	}
}
