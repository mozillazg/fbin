.PHONY: help
help:
	@echo "lint             run lint"
	@echo "release          compile for all platforms"
	@echo "build            build"

PROJECT=fbin
VERSION=$(shell cat cmd/fbin/main.go |grep 'version = "[0-9]\+.[0-9]\+.[0-9]\+"' | awk -F '"' '{print $$2}')
GIT_COMMIT=$(shell git rev-parse --short HEAD)
BUILT_TIME=$(shell date -u '+%FT%T%z')

GOVERSION=$(shell go version)
GOOS=$(word 1,$(subst /, ,$(lastword $(GOVERSION))))
GOARCH=$(word 2,$(subst /, ,$(lastword $(GOVERSION))))
LDFLAGS="-X main.gitCommit=${GIT_COMMIT} -X main.built=${BUILT_TIME}"

ARCNAME=$(PROJECT)-$(VERSION)-$(GOOS)-$(GOARCH)
RELDIR=$(ARCNAME)

DISTDIR=dist
export GO111MODULE=on

.PHONY: _release
_release:
	rm -rf $(DISTDIR)/$(RELDIR)
	mkdir -p $(DISTDIR)/$(RELDIR)
	go clean
	GOOS=$(GOOS) GOARCH=$(GOARCH) go build -ldflags ${LDFLAGS} ./cmd/fbin
	cp $(PROJECT)$(SUFFIX_EXE) $(DISTDIR)/$(RELDIR)/
	tar czf $(DISTDIR)/$(ARCNAME).tar.gz -C $(DISTDIR) $(RELDIR)
	go clean

.PHONY: release
release:
	# https://golang.org/doc/install/source#environment
	# @$(MAKE) _release GOOS=android   GOARCH=arm
	@$(MAKE) _release GOOS=darwin    GOARCH=386
	@$(MAKE) _release GOOS=darwin    GOARCH=amd64
	# @$(MAKE) _release GOOS=darwin    GOARCH=arm
	# @$(MAKE) _release GOOS=darwin    GOARCH=arm64
	@$(MAKE) _release GOOS=dragonfly GOARCH=amd64
	@$(MAKE) _release GOOS=freebsd   GOARCH=386
	@$(MAKE) _release GOOS=freebsd   GOARCH=amd64
	@$(MAKE) _release GOOS=freebsd   GOARCH=arm
	@$(MAKE) _release GOOS=linux     GOARCH=386
	@$(MAKE) _release GOOS=linux     GOARCH=amd64
	@$(MAKE) _release GOOS=linux     GOARCH=arm
	@$(MAKE) _release GOOS=linux     GOARCH=arm64
	@$(MAKE) _release GOOS=linux     GOARCH=ppc64
	@$(MAKE) _release GOOS=linux     GOARCH=ppc64le
	@$(MAKE) _release GOOS=linux     GOARCH=mips
	@$(MAKE) _release GOOS=linux     GOARCH=mipsle
	@$(MAKE) _release GOOS=linux     GOARCH=mips64
	@$(MAKE) _release GOOS=linux     GOARCH=mips64le
	@$(MAKE) _release GOOS=linux     GOARCH=s390x
	@$(MAKE) _release GOOS=netbsd    GOARCH=386
	@$(MAKE) _release GOOS=netbsd    GOARCH=amd64
	@$(MAKE) _release GOOS=netbsd    GOARCH=arm
	@$(MAKE) _release GOOS=openbsd   GOARCH=386
	@$(MAKE) _release GOOS=openbsd   GOARCH=amd64
	@$(MAKE) _release GOOS=openbsd   GOARCH=arm
	# @$(MAKE) _release GOOS=plan9     GOARCH=386
	# @$(MAKE) _release GOOS=plan9     GOARCH=amd64
	# @$(MAKE) _release GOOS=solaris   GOARCH=amd64
	@$(MAKE) _release GOOS=windows   GOARCH=amd64 SUFFIX_EXE=.exe
	@$(MAKE) _release GOOS=windows   GOARCH=386   SUFFIX_EXE=.exe

.PHONY: build
build:
	go build ./cmd/fbin

.PHONY: lint
lint:
	gofmt -s -w .
	golint .
	go vet

.PHONY: clean
clean:
	go clean
