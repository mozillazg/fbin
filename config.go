package fbin

import (
	"io/ioutil"

	"github.com/BurntSushi/toml"
	"github.com/mozillazg/fbin/storage"
)

type Config struct {
	Service serviceConfig `toml:"service"`
	Storage storageConfig `toml:"storage"`
}

type serviceConfig struct {
	Listen string     `toml:"listen"`
	Auth   authConfig `toml:"auth"`

	MaxFormMemory int64 `toml:"max_form_memory"`
	MaxBodySize   int64 `toml:"max_body_size"`
}

type storageConfig struct {
	MainService string                    `toml:"main_service"`
	Services    map[string]storage.Option `toml:"services"`
}

type authConfig struct {
	Token string `toml:"token"`
}

func UnmarshalFile(f string, c *Config) error {
	b, err := ioutil.ReadFile(f)
	if err != nil {
		return err
	}

	return toml.Unmarshal(b, c)
}

func (sc serviceConfig) MaxFormMemoryB() int64 {
	if sc.MaxFormMemory == 0 {
		return defaultMaxFormMemory
	}

	return sc.MaxFormMemory * 1024 * 1024
}

func (sc serviceConfig) MaxBodySizeB() int64 {
	if sc.MaxBodySize == 0 {
		return defaultMaxBodySize
	}

	return sc.MaxBodySize * 1024 * 1024
}
